﻿using System;
using System.Text.RegularExpressions;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace FlickrAutomation
{
    public class Page
    {
        static string PageTitle = "Find your inspiration. | Flickr";

        public static void Goto()
        {
            Driver.Instance.Navigate().GoToUrl("http://flickr.com");
        }

        public static bool IsAt()
        {
            return Driver.Instance.Title == PageTitle;
        }

        public static Search SearchAs(string searchWords)
        {
            return new Search (searchWords);
        }

        public static void ChoseFilter()
        {
            var filterLicense = Driver.Instance.FindElement(By.ClassName("filter-license"));
            filterLicense.Click();
            var filterAllElements = Driver.Instance.FindElement(By.ClassName("droparound"));
            var filterElement = filterAllElements.FindElements(By.CssSelector("li"))[5];
            filterElement.Click();
            Driver.TimeOut.Until(ExpectedConditions.InvisibilityOfElementLocated(By.ClassName("dirty")));
        }
        public static bool MoreTotal()
        {
            var searchResult = Driver.Instance.FindElement(By.ClassName("view-more-link"));
            int count = int.Parse(Regex.Replace(searchResult.Text, @"[^\d]", String.Empty));
            if (count > 1000)
                return true;
            return false;
        }
    }

    public class Search
    {
        private readonly string searchWords;

        public Search(string searchWords)
        {
            this.searchWords = searchWords;
        }
        public void Enter()
        {
            var searchBox = Driver.Instance.FindElement(By.Id("search-field"));
            searchBox.SendKeys(searchWords);
            Thread.Sleep(2000);
            searchBox.SendKeys(Keys.Enter);
            Driver.TimeOut.Until(ExpectedConditions.ElementIsVisible(By.ClassName("search-photos-results")));
        }
    }
}