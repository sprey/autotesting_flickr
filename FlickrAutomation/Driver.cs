﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;

namespace FlickrAutomation
{
    public class Driver
    {
        public static IWebDriver Instance
        {
            get;
            set;
        }
        public static WebDriverWait TimeOut
        {
            get;
            set;
        }

        public static void Initialize()
        {
            Instance = new ChromeDriver(@"C:\\libs\");
            TimeOut = new WebDriverWait(Instance, TimeSpan.FromSeconds(10));
        }

        public static void Close()
        {
            Instance.Close();
        }

    }

}