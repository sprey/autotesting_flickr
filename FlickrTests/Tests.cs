﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FlickrAutomation;

namespace FlickrTests
{
    [TestClass]
    public class Tests
    {
        [TestInitialize]
        public void Init()
        {
            Driver.Initialize();
        }

        [TestMethod]
        public void Can_Go_To_HomePage()
        {
            Page.Goto();
            Assert.IsTrue(Page.IsAt(), "Failed to homepage");
        }

        [TestMethod]
        public void Total_Count_More_1000()
        {
            Page.Goto();
            Page.SearchAs("cats").Enter();
            Page.ChoseFilter();
            Assert.IsTrue(Page.MoreTotal(), "Total count less than 1000");
        }

        [TestCleanup]
        public void CleanUp()
        {
            Driver.Close();
        }
    }
}
